<%@ page language="java" import="java.util.*" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<html>
<head>
	<title>Login successful</title>
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/styles/main.css" />
	<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
</head>
<body>
	<%@ include file="/layout/header.jspf"%>
	<br><br> 
	<div align="center">
		<b>Welcome!</b>
		<br>
		Init params: [${model.name} /  ${model.email}]<br>
		POST params: [${model.username} / ${model.password}]<br>
		<c:if test="${not empty model.param1}">
		   GET params:  [${model.param1}]
		</c:if>
	</div>
	<br><br> 
	<%@ include file="/layout/footer.jspf"%>

</body>
</html>