<%@ page language="java" import="java.util.*" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<html>
<head>
	<title>JSP login page</title>
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/styles/main.css" />
</head>
<body>
	<%@ include file="/layout/header.jspf"%>
	<br><br> 
	<div align="center">
		<form action="${pageContext.request.contextPath}/login" method="post">
			Username:<input type="text" name="username"/><br><br> 
			Password:<input type="password" name="password"/><br><br> 
			<input type="submit" value="login"/>
		</form>
	</div>
	<br><br> 
	<jsp:include page="/layout/footer.jspf"/>
</body>
</html>