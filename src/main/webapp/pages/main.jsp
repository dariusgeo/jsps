<%@ page language="java" import="java.util.*" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<html>
<head>
<title>JSP page</title>
<!-- Use this for demonstrating useBean purpose - get attribute from the given scope -->
<!-- jsp:useBean id="stringList" type="java.util.List" scope="request" / -->
 <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/styles/main.css"/> 
</head>
<body>

	<%!List<String> stringList = new ArrayList<String>();%>

	<%!public void jspInit() {
		stringList.add("AAAA");
		stringList.add("BBBB");
		stringList.add("CCCC");
	}%>

	<!-- JSP Directive -->
	<%@ include file="/layout/header.jspf"%>
	<c:forEach items="<%=stringList%>" var="element">
		<c:out value="${element}"/><br>
	</c:forEach>

	<!-- JSP Standard Action -->
	<jsp:include page="/layout/footer.jspf" />

</body>
</html>