package sda.servlet;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class LoginServlet extends HttpServlet {
	
	private static final long serialVersionUID = 1L;

	private String name = null;
	private String email = null;
	
	@Override
	public void init(ServletConfig config) throws ServletException {
		name = config.getInitParameter("name");
		email = config.getInitParameter("email");
	}
	
	
	@Override
	protected void service(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		super.service(req, resp);
	}
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {	
		handleRequest(req, resp);
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {		
		handleRequest(req, resp);
	}

	@Override
	public void destroy() {
		// Close opened resources: DB connections, streams, etc..
	}
		
	private void handleRequest(HttpServletRequest req, HttpServletResponse resp) {

		String username =req.getParameter("username");
		String password =req.getParameter("password");
		String param1 = req.getParameter("param1");
		
		try {
			Map<String, String>  model = new HashMap<String, String>();
			model.put("username", username);
			model.put("password", password);
			model.put("param1", param1);
			model.put("name", name);
			model.put("email", email);
			req.setAttribute("model", model);
			req.getRequestDispatcher("pages/loginSuccessful.jsp").forward(req, resp);
			
		} catch (IOException | ServletException e) {
			e.printStackTrace();
		}
	}

}